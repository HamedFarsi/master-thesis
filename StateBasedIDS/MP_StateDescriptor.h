#pragma once
#include <QCoreApplication>
#include <QVector>
#include <QMap>
#include <QBitArray>

class MP_StateDescriptor
{
public:
	typedef quint16 HoldingRegister;
	typedef quint16	InputRegister;
public:
	struct PrimaryTables
	{
		QBitArray mCoils;
		QBitArray mDiscreteInputs;
		QVector<HoldingRegister> mHoldingRegisters;
		QVector<InputRegister> mInputRegisters;
	};
	struct NodeSelector
	{
		quint32 IP;
		quint16 Port;
		quint8	unitID;
		NodeSelector():IP(0),Port(0),unitID(0){}
		NodeSelector(quint32 p_IP, quint16 p_Port , quint8 p_unitID):
		IP(p_IP),Port(p_Port),unitID(p_unitID){}

		bool operator  < (const NodeSelector & n)const
		{
			if(Port < n.Port)
				return true;
			else if(Port > n.Port)
				return false;
			if(IP < n.IP)
				return true;
			else if(IP > n.IP)
				return false;
			if(unitID < n.unitID)
				return true;
			else if(unitID > n.unitID)
				return false;
			return false;
		}
		bool operator == (NodeSelector & pf)
		{
			if (IP == pf.IP && Port == pf.Port && unitID == pf.unitID)
				return true;
			else
				return false;
		}
	};
	enum ColiState
	{
		OFF = 0x0000,
		ON = 0xFF00
	};
	struct PrimaryFeatures
	{
		QVector<quint16> EssentialCO;
		QVector<quint16> EssentialDI;
		QVector<quint16> EssentialHR;
		QVector<quint16> EssentialIR;
		NodeSelector	EssentialTablesNode;

	};
public:
	typedef QMap<NodeSelector,PrimaryTables> SystemStateTable;
public:
	SystemStateTable mSystemState;
	quint16 mMaxHoldingRegisterValue;
	quint16 mMaxInputRegisterValue;
	QVector<PrimaryFeatures> mPrimaryFeatures;
	
public:
	MP_StateDescriptor(void);
	~MP_StateDescriptor(void);
	void ParseAndLoadINIFile(char * FilePath);
	void ParseImportantFeatures(char * FilePath);

};
