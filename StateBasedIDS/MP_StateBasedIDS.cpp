#include "MP_StateBasedIDS.h"
#include "MP_ParallelRun.h"
#include <math.h>

MP_StateBasedIDS::MP_StateBasedIDS(MP_Interface * Iface):mIface(Iface)
{
	init();
}

MP_StateBasedIDS::~MP_StateBasedIDS(void)
{
	while (!ThreadList.isEmpty())
	{
		delete ThreadList.first();
	}
}

void MP_StateBasedIDS::init()
{
	mIface->init();
	for (int i = 0 ; i < MP_NUM_OF_PROCESSORS ; ++i)
	{
		MP_ParallelRun * Pr = new MP_ParallelRun(this);
		ThreadList.append(Pr);
	}
}


void MP_StateBasedIDS::MainProcess(MP_Packet * packet)
{
	QMutexLocker ml(&mStateMonitor.mSystemStateRWL);
	mStateMonitor.MonitorAndUpdateState(packet);
	IsCriticalAndComputeDistance(packet);
	mStateMonitor.ConstructDataSet(packet);
}
int MP_StateBasedIDS::IsCriticalAndComputeDistance(MP_Packet * packet )//TODO: is mCriticalState needs readlocker?
{
	//QReadLocker rl(&mStateMonitor.mSystemStateRWL);
	double sum = 0 ;
	quint16 critical_count = 0;
	int is_critical = 0;
	MP_StateDescriptor::NodeSelector selector(packet->ip_get_src(),packet->tcp_get_src_port(),packet->modbus_get_unitID());
	MP_StateDescriptor::SystemStateTable::Iterator primary_table_itr = mStateMonitor.mStateDescriptor.mSystemState.find(selector);
	if(primary_table_itr == mStateMonitor.mStateDescriptor.mSystemState.end())
		return -1;
	MP_CriticalState::CriticalStates::Iterator cs_itr = mCriticalState.mCriticalStates.find(selector);
	if(cs_itr == mCriticalState.mCriticalStates.end())
		return -1;
	for (int i = 0 ; i < cs_itr->CSRule.size() ; ++i)
	{
		for (int j = 0 ; j < cs_itr->CSRule[i].size() ; ++j)
		{
			if(cs_itr->CSRule[i][j].sTableType == MP_CriticalState::CO)
			{
				bool bit = false;
				if(cs_itr->CSRule[i][j].sConstraint == 0)
					bit = false;
				else if(cs_itr->CSRule[i][j].sConstraint == 1)
					bit = true;

				switch (cs_itr->CSRule[i][j].sOp)
				{
				case MP_CriticalState::Equall:
					{
						if(primary_table_itr->mCoils.testBit(cs_itr->CSRule[i][j].sIndex) == bit)
						{
							++critical_count;
						}
						else
						{
							sum += 1 ;
						}
					}
					break;
				case MP_CriticalState::NotEquall:
					{
						if(primary_table_itr->mCoils.testBit(cs_itr->CSRule[i][j].sIndex) != bit)
						{
							++critical_count;
						}
						else
						{
							sum += 1;
						}
					}
					break;
				default:Q_ASSERT(false);
				}
			}
			else if(cs_itr->CSRule[i][j].sTableType == MP_CriticalState::DI)
			{
				bool bit = false;
				if(cs_itr->CSRule[i][j].sConstraint == 0)
					bit = false;
				else if(cs_itr->CSRule[i][j].sConstraint == 1)
					bit = true;

				switch (cs_itr->CSRule[i][j].sOp)
				{
				case MP_CriticalState::Equall:
					{
						if(primary_table_itr->mDiscreteInputs.testBit(cs_itr->CSRule[i][j].sIndex) == bit)
						{
							++critical_count;
						}
						else
						{
							sum += 1;
						}
					}
					break;
				case MP_CriticalState::NotEquall:
					{
						if(primary_table_itr->mDiscreteInputs.testBit(cs_itr->CSRule[i][j].sIndex) != bit)
						{
							++critical_count;
						}
						else
						{
							sum += 1;
						}
					}
					break;
				default:Q_ASSERT(false);
				}
			}
			else if(cs_itr->CSRule[i][j].sTableType == MP_CriticalState::HR)
			{
				switch (cs_itr->CSRule[i][j].sOp)
				{
				case MP_CriticalState::Equall:
					{
						if(primary_table_itr->mHoldingRegisters[cs_itr->CSRule[i][j].sIndex] == cs_itr->CSRule[i][j].sConstraint)
						{
							++critical_count;
						}
						else
						{
							double const_norm = cs_itr->CSRule[i][j].sConstraint/mStateMonitor.mMaxHoldingRegisterValue;
							double reg_norm = primary_table_itr->mHoldingRegisters[cs_itr->CSRule[i][j].sIndex]/mStateMonitor.mMaxHoldingRegisterValue;
							double res = abs(reg_norm - const_norm);
							sum += res;
						}
					}
					break;
				case MP_CriticalState::NotEquall:
					{
						if(primary_table_itr->mHoldingRegisters[cs_itr->CSRule[i][j].sIndex] != cs_itr->CSRule[i][j].sConstraint)
						{
							++critical_count;
						}
						else
						{
							sum += 0;//TODO
						}
					}
					break;
				case MP_CriticalState::Larger:
					{
						if(primary_table_itr->mHoldingRegisters[cs_itr->CSRule[i][j].sIndex] > cs_itr->CSRule[i][j].sConstraint)
						{
							++critical_count;
						}
						else
						{
							double const_norm = cs_itr->CSRule[i][j].sConstraint/mStateMonitor.mMaxHoldingRegisterValue;
							double reg_norm = primary_table_itr->mHoldingRegisters[cs_itr->CSRule[i][j].sIndex]/mStateMonitor.mMaxHoldingRegisterValue;
							double res = abs(reg_norm - const_norm);
							sum += res;
						}
					}
					break;
				case MP_CriticalState::LargerAndEqual:
					{
						if(primary_table_itr->mHoldingRegisters[cs_itr->CSRule[i][j].sIndex] >= cs_itr->CSRule[i][j].sConstraint)
						{
							++critical_count;
						}
						else
						{
							double const_norm = cs_itr->CSRule[i][j].sConstraint/mStateMonitor.mMaxHoldingRegisterValue;
							double reg_norm = primary_table_itr->mHoldingRegisters[cs_itr->CSRule[i][j].sIndex]/mStateMonitor.mMaxHoldingRegisterValue;
							double res = abs(reg_norm - const_norm);
							sum += res;
						}
					}
					break;
				case MP_CriticalState::Smaller:
					{
						if(primary_table_itr->mHoldingRegisters[cs_itr->CSRule[i][j].sIndex] < cs_itr->CSRule[i][j].sConstraint)
						{
							++critical_count;
						}
						else
						{
							double const_norm = cs_itr->CSRule[i][j].sConstraint/mStateMonitor.mMaxHoldingRegisterValue;
							double reg_norm = primary_table_itr->mHoldingRegisters[cs_itr->CSRule[i][j].sIndex]/mStateMonitor.mMaxHoldingRegisterValue;
							double res = abs(reg_norm - const_norm);
							sum += res;
						}

					}
					break;
				case MP_CriticalState::SmallerAndEqual:
					{
						if(primary_table_itr->mHoldingRegisters[cs_itr->CSRule[i][j].sIndex] <= cs_itr->CSRule[i][j].sConstraint)
						{
							++critical_count;
						}
						else
						{
							double const_norm = cs_itr->CSRule[i][j].sConstraint/mStateMonitor.mMaxHoldingRegisterValue;
							double reg_norm = primary_table_itr->mHoldingRegisters[cs_itr->CSRule[i][j].sIndex]/mStateMonitor.mMaxHoldingRegisterValue;
							double res = abs(reg_norm - const_norm);
							sum += res;
						}
					}
					break;
				default:Q_ASSERT(false);
				}
			}
 			else if(cs_itr->CSRule[i][j].sTableType == MP_CriticalState::IR)
			{
				switch (cs_itr->CSRule[i][j].sOp)
				{
				case MP_CriticalState::Equall:
					{
						if(primary_table_itr->mInputRegisters[cs_itr->CSRule[i][j].sIndex] == cs_itr->CSRule[i][j].sConstraint)
						{
							++critical_count;
						}
						else
						{
							double const_norm = cs_itr->CSRule[i][j].sConstraint/mStateMonitor.mMaxInputRegisterValue;
							double reg_norm = primary_table_itr->mInputRegisters[cs_itr->CSRule[i][j].sIndex]/mStateMonitor.mMaxInputRegisterValue;
							double res = abs(reg_norm - const_norm);
							sum += res;
						}
					}
					break;
				case MP_CriticalState::NotEquall:
					{
						if(primary_table_itr->mInputRegisters[cs_itr->CSRule[i][j].sIndex] != cs_itr->CSRule[i][j].sConstraint)
						{
							++critical_count;
						}
						else
						{
							sum += 0;//TODO
						}
					}
					break;
				case MP_CriticalState::Larger:
					{
						if(primary_table_itr->mInputRegisters[cs_itr->CSRule[i][j].sIndex] > cs_itr->CSRule[i][j].sConstraint)
						{
							++critical_count;
						}
						else
						{
							double const_norm = cs_itr->CSRule[i][j].sConstraint/mStateMonitor.mMaxInputRegisterValue;
							double reg_norm = primary_table_itr->mInputRegisters[cs_itr->CSRule[i][j].sIndex]/mStateMonitor.mMaxInputRegisterValue;
							double res = abs(reg_norm - const_norm);
							sum += res;
						}
					}
					break;
				case MP_CriticalState::LargerAndEqual:
					{
						if(primary_table_itr->mInputRegisters[cs_itr->CSRule[i][j].sIndex] >= cs_itr->CSRule[i][j].sConstraint)
						{
							++critical_count;
						}
						else
						{
							double const_norm = cs_itr->CSRule[i][j].sConstraint/mStateMonitor.mMaxInputRegisterValue;
							double reg_norm = primary_table_itr->mInputRegisters[cs_itr->CSRule[i][j].sIndex]/mStateMonitor.mMaxInputRegisterValue;
							double res = abs(reg_norm - const_norm);
							sum += res;
						}
					}
					break;
				case MP_CriticalState::Smaller:
					{
						if(primary_table_itr->mInputRegisters[cs_itr->CSRule[i][j].sIndex] < cs_itr->CSRule[i][j].sConstraint)
						{
							++critical_count;
						}
						else
						{
							double const_norm = cs_itr->CSRule[i][j].sConstraint/mStateMonitor.mMaxInputRegisterValue;
							double reg_norm = primary_table_itr->mInputRegisters[cs_itr->CSRule[i][j].sIndex]/mStateMonitor.mMaxInputRegisterValue;
							double res = abs(reg_norm - const_norm);
							sum += res;
						}
					}
					break;
				case MP_CriticalState::SmallerAndEqual:
					{
						if(primary_table_itr->mInputRegisters[cs_itr->CSRule[i][j].sIndex] <= cs_itr->CSRule[i][j].sConstraint)
						{
							++critical_count;
						}
						else
						{
							double const_norm = cs_itr->CSRule[i][j].sConstraint/mStateMonitor.mMaxInputRegisterValue;
							double reg_norm = primary_table_itr->mInputRegisters[cs_itr->CSRule[i][j].sIndex]/mStateMonitor.mMaxInputRegisterValue;
							double res = abs(reg_norm - const_norm);
							sum += res;
						}
					}
					break;
				default:Q_ASSERT(false);
				}
			}
		}
		if(critical_count == cs_itr->CSRule[i].size())
		{
			is_critical = 1;
		}
		mCriticalDistanceMutex.lock();
		mCriticalDistance.push_back(sqrt(sum));
		mCriticalDistanceMutex.unlock();

		sum = 0;
	}
	return is_critical;
}

void MP_StateBasedIDS::start()
{
	for (int i = 0 ; i < MP_NUM_OF_PROCESSORS ; ++i)
	{
		ThreadList[i]->start();
	}

}
