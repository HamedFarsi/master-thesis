#pragma once
#include <QCoreApplication>
class MP_StateBasedIDS;
class MP_Interface;
class MP_ParallelRun;
class MP_StateBasedIDSCore :public QCoreApplication
{
public:
	MP_StateBasedIDS * mStateBasedIDS;
	MP_Interface * mInterface;
	MP_ParallelRun * mParallelRun;
public:
	MP_StateBasedIDSCore(int &argc, char **argv);
	~MP_StateBasedIDSCore(void);
	void init();
};
