#pragma once
#include <QThread>
class MP_StateBasedIDS;

class MP_ParallelRun : public QThread
{
public:
	MP_StateBasedIDS * StateBasedIDS;
public:
	MP_ParallelRun(MP_StateBasedIDS *);
	~MP_ParallelRun(void);
	void run();
	void begin();
	

};
