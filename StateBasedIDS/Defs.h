#define MP_MAX_PACKET_LEN			2048
#define MP_MAX_PACKET_QUEUE_SIZE	2000
#define MP_NUM_OF_PROCESSORS		8
#define MP_ETH_IPV4_HDR_OFF			14
#define MP_IPV4_TOTAL_LEN_OFF		16
#define MP_IPV4_PROTOCOL_OFF		23
#define MP_ETH_IPV4_SRC_OFF			26
#define MP_ETH_IPV4_DST_OFF			30
#define MP_REV_MODBUS_PORT		    0xF601 //502
#define MP_MODBUS_TCP_HEADER_LEN	7
#define MP_EXEPTION_RESPONSE_MASK	0x80
#define MP_NORMAL_RESPONSE			0x00
#define MP_EXEPTION_RESPONSE		0x80
#define MP_NUM_OF_ENTITY			10
#define MP_INI_SYSTEM_INITIAL_FILE_PATH			"./system_state.ini"
#define MP_INI_SYSTEM_IMPORTANT_FEATURES_FILE_PATH			"./main_vars.ini"
#define MP_INI_CRITICAL_STATE_PATH				"./critical_states.ini"
#define MP_NUM_OF_THREADS			8
#define MP_DATASET_CONSTRUCTION
/////////////////////////////////////////////////////
#define MP_NUMBER_OF_HEATERS	3

#define		Heater						28
#define		WholeShutdown				3
#define		SaftyInputValve				0
#define		HPAlarm						1
#define		SteamValve					2
#define		SFR							0
#define		InputValve					0
#define		OutputValve					1
#define		Temprature					2
#define		Pressure					3
#define		InputFlowRate				4
#define		OutputFlowRate				5
#define		TankLevel					6
#define		TSP							7
#define		LSP							8
#define		TUpperBoundSP				9
#define		TLowerBoundSP				10
#define		LUpperBoundSP				11
#define		LLowerBoundSP				12
#define		TPGain						13
#define		TIGain						14
#define		TDGain						15
#define		IN_LPGain					16
#define		IN_LIGain					17
#define		IN_LDGain					18
#define		OUT_LPGain					23
#define		OUT_LIGain					24
#define		OUT_LDGain					25
#define		TPIDOutputLowerBound		20
#define		TPIDOutputUpperBound		19
#define		IN_LPIDOutputLowerBound		20
#define		IN_LPIDOutputUpperBound		21
#define		OUT_LPIDOutputLowerBound	27
#define		OUT_LPIDOutputUpperBound	26
/////////////////////////////////////////////////////
#define MP_uint32_reverse(p) ((unsigned int)(((p&0xff)<<24)|((p&0xff00)<<8)|(((unsigned int)(p)&0xff0000)>>8)|(((unsigned int)(p)&0xff000000)>>24)))
#define MP_uint16_reverse(x) ( (((x) & 0xff) << 8) | ((unsigned short)(x) >> 8) )