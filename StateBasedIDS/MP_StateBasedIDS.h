#pragma once
#include "MP_StateMonitor.h"
#include "MP_CriticalState.h"
class MP_ParallelRun;
class MP_StateBasedIDS
{

public:
	MP_StateMonitor mStateMonitor;
	QVector<MP_ParallelRun *> ThreadList;
	MP_Interface * mIface;
	MP_CriticalState mCriticalState;
	QVector<double> mCriticalDistance;
	QMutex mCriticalDistanceMutex;
public:
	MP_StateBasedIDS(MP_Interface *);
	~MP_StateBasedIDS(void);
	void init();
	void start();
	void MainProcess(MP_Packet *);
	int IsCriticalAndComputeDistance(MP_Packet * packet);
	

};
