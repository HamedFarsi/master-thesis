#pragma once
#include <QVector>
#include "MP_StateDescriptor.h"

class MP_CriticalState
{
public:
	enum TableType
	{
		CO = 1,
		DI = 2,
		HR = 3,
		IR = 4
	};
	enum ConstraintOperator
	{
		Larger			= 1,
		LargerAndEqual  = 2,
		Smaller			= 3,
		SmallerAndEqual = 4,
		Equall			= 5,
		NotEquall		= 6
	};
	struct PureRule
	{
		quint8 sTableType;
		quint8 sOp;
		quint16 sIndex;
		quint16 sConstraint;
		PureRule():sTableType(0),sOp(0),sIndex(0),sConstraint(0){}
	};
	struct CSRuleStruct
	{
		QVector<QVector<PureRule>> CSRule;
	};
// 	struct CSSelector
// 	{
// 		quint32 ip;
// 		quint16 port;
// 		quint8 unitID;
// 		CSSelector():ip(0),port(0),unitID(0){}
// 		bool operator < (const CSSelector cs)const
// 		{
// 			if(ip < cs.ip)
// 				return true;
// 			else if(ip > cs.ip)
// 				return false;
// 			else if(port < cs.port)
// 				return true;
// 			else if(port > cs.port)
// 				return false;
// 			else
// 				return false;
// 		}
// 	};
public:
	typedef QMap<MP_StateDescriptor::NodeSelector,CSRuleStruct> CriticalStates;
public:
	CriticalStates mCriticalStates;
public:
	MP_CriticalState(void);
	~MP_CriticalState(void);
};
