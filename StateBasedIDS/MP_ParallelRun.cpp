#include "MP_ParallelRun.h"
#include "MP_StateBasedIDS.h"

MP_ParallelRun::MP_ParallelRun(MP_StateBasedIDS * IDS):StateBasedIDS(IDS)
{
}

MP_ParallelRun::~MP_ParallelRun(void)
{
}

void MP_ParallelRun::run()
{
	while (StateBasedIDS->mIface->running)
	{
		MP_Packet *  packet = NULL;
		packet = StateBasedIDS->mIface->ReadPacket();
		if(packet == NULL)
		{
			Sleep(10);
			continue;
		}
		if(!packet->is_modbus_packet())
		{
			//TODO: do some logging
			free(packet->pkt);
			free(packet);
			continue;
		}
		if ((packet->tcp_get_flags() & 0x02) == 0x02 || (packet->tcp_get_flags() & 0x12) == 0x12)
		{
			free(packet->pkt);
			free(packet);
			continue;
		}
		if ((packet->tcp_get_flags() == 0x10) && ((packet->ip_get_total_len()-packet->ip_get_header_len()-packet->tcp_get_header_len())==0) )
		{
			free(packet->pkt);
			free(packet);
			continue;
		}
		if (((packet->tcp_get_flags() & 0x01) == 0x01))
		{
			free(packet->pkt);
			free(packet);
			continue;
		}
		if ((packet->tcp_get_flags() & 0x04) == 0x04)
		{
			free(packet->pkt);
			free(packet);
			continue;
		}
		StateBasedIDS->MainProcess(packet);
		free(packet->pkt);
		free(packet);
	}
}

void MP_ParallelRun::begin()
{
	StateBasedIDS->start();
}
