#include <QSettings>
#include <QHostAddress>
#include <QStringList>
#include "MP_StateDescriptor.h"
#include "Defs.h"



MP_StateDescriptor::MP_StateDescriptor(void):mMaxInputRegisterValue(0),mMaxHoldingRegisterValue(0)
{
	ParseAndLoadINIFile(MP_INI_SYSTEM_INITIAL_FILE_PATH);
	ParseImportantFeatures(MP_INI_SYSTEM_IMPORTANT_FEATURES_FILE_PATH);

}

MP_StateDescriptor::~MP_StateDescriptor(void)
{
}

void MP_StateDescriptor::ParseAndLoadINIFile( char * FilePath )
{
	QSettings settings(FilePath,QSettings::IniFormat);
	int node_count = settings.value("size").toInt();
	for (int i = 0 ; i < node_count ; ++i )
	{
		NodeSelector sel;
		PrimaryTables tables;
		settings.beginGroup("node_"+ QString::number(i+1));
		QHostAddress ip(settings.value("ip").toString());
		sel.IP = ip.toIPv4Address();
		sel.Port = settings.value("port").toUInt();
		sel.unitID = settings.value("unit_id").toUInt();
		int size = settings.value("coils_num").toUInt();
		tables.mCoils.resize(size);
		QStringList vals = settings.value("coils_value").toString().split("-");
		for(int  j = 0 ; j < size ; ++j)
		{
			if(vals.at(j) == "0")
				tables.mCoils.clearBit(j);
			else
				tables.mCoils.setBit(j);
		}
		vals.clear();
		size = settings.value("dicrete_input_num").toUInt();
		tables.mDiscreteInputs.resize(size);
		vals = settings.value("dicrete_input_value").toString().split("-");
		for(int  j = 0 ; j < size ; ++j)
		{
			if(vals.at(j) == "0")
				tables.mDiscreteInputs.clearBit(j);
			else
				tables.mDiscreteInputs.setBit(j);
		}
		vals.clear();
		size = settings.value("input_register_num").toUInt();
		mMaxInputRegisterValue = settings.value("max_input_register").toUInt();
		vals = settings.value("input_register_value").toString().split("-");
		for(int  j = 0 ; j < size ; ++j)
		{
			tables.mInputRegisters.append(vals.at(j).toUShort());
		}
		vals.clear();
		size = settings.value("holding_register_num").toUInt();
		mMaxHoldingRegisterValue = settings.value("max_holding_register").toUInt();
		vals = settings.value("holding_register_value").toString().split("-");
		for(int  j = 0 ; j < size ; ++j)
		{
			tables.mHoldingRegisters.append(vals.at(j).toUShort());
		}
		settings.endGroup();
		mSystemState.insert(sel,tables);
	}

}

void MP_StateDescriptor::ParseImportantFeatures(char * FilePath)
{
	QSettings settings(FilePath, QSettings::IniFormat);
	int node_count = settings.value("size").toInt();
	PrimaryFeatures pf;
	for (int i = 0; i < node_count; ++i)
	{
		QStringList strList = settings.value("ip").toString().split(":");
		pf.EssentialTablesNode.IP = QHostAddress(strList.at(0)).toIPv4Address();
		pf.EssentialTablesNode.Port= strList.at(1).toUShort();
		pf.EssentialTablesNode.unitID = strList.at(2).toUShort();
		QString tmp = settings.value("HR").toString();
		if (tmp != "Nan")
		{
			strList = tmp.split("-");
			for (int k = 0; k < strList.size(); ++k)
			{
				pf.EssentialHR.append(strList.at(k).toUShort());
			}
		}
		tmp = settings.value("DI").toString();
		if (tmp != "NaN")
		{
			strList = tmp.split("-");
			for (int k = 0; k < strList.size(); ++k)
			{
				pf.EssentialDI.append(strList.at(k).toUShort());
			}
		}		
		tmp = settings.value("IR").toString();
		if (tmp != "Nan")
		{
			strList = tmp.split("-");
			for (int k = 0; k < strList.size(); ++k)
			{
				pf.EssentialIR.append(strList.at(k).toUShort());
			}
		}
		tmp = settings.value("CO").toString();
		if (tmp != "Nan")
		{
			strList = tmp.split("-");
			for (int k = 0; k < strList.size(); ++k)
			{
				pf.EssentialCO.append(strList.at(k).toUShort());
			}
		}
		mPrimaryFeatures.append(pf);	
	}
}

