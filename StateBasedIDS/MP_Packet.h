#pragma once
#include <QCoreApplication>
#include "Defs.h"
class MP_Packet
{
public:
	enum FunctionCodes
	{
		Read_Coils						 = 0x01,
		Read_Discrete_Inputs			 = 0x02,
		Read_Holding_Registers			 = 0x03,
		Read_Input_Registers			 = 0x04,
		Write_Single_Coil				 = 0x05,
		Write_Single_Register			 = 0x06,
		Read_Exception_Status			 = 0x07,
		Diagnostics						 = 0x08,
		Get_Comm_Event_Counter			 = 0x0B,
		Get_Comm_Event_Log				 = 0x0C,
		Write_Multiple_Coils			 = 0x0F,
		Write_Multiple_Registers		 = 0x10,
		Report_Server_ID				 = 0x11,
		Read_File_Record			     = 0x14,
		Write_File_Record				 = 0x15,
		Mask_Write_Register				 = 0x16,
		Read_Write_Multiple_registers	 = 0x17,
		Read_FIFO_Queue					 = 0x18,
		Encapsulated_Interface_Transport = 0x2B
	};
public:
	unsigned char * pkt;
public:
	MP_Packet(void);
	~MP_Packet(void);
	inline quint16 get_total_len()
	{
		return (MP_ETH_IPV4_HDR_OFF + ip_get_total_len());
	}
	inline quint8	ip_get_version () const
	{
		return ((*(pkt+MP_ETH_IPV4_HDR_OFF))&0xf0) >> 4 ;
	}

	inline quint8	ip_get_header_len () const
	{
		return quint8(((*(pkt+ MP_ETH_IPV4_HDR_OFF))&0xf)<<2);
	}
	inline quint16	ip_get_total_len () const
	{
		const quint16 rtotal_len = *(quint16*)(pkt + MP_IPV4_TOTAL_LEN_OFF );
		return MP_uint16_reverse ( rtotal_len);
	}
	inline quint8	ip_get_protocol () const
	{
		return *(pkt + MP_IPV4_PROTOCOL_OFF);
	}
	inline quint32	ip_get_r_src () const
	{
		return (*((quint32 *) (pkt + MP_ETH_IPV4_SRC_OFF)));

	}
	inline quint32	ip_get_src () const
	{
		return MP_uint32_reverse(*((quint32 *) (pkt + MP_ETH_IPV4_SRC_OFF)));

	}
	inline quint32	ip_get_r_dst () const
	{
		return (*((quint32 *) (pkt + MP_ETH_IPV4_DST_OFF)));

	}
	inline quint32	ip_get_dst () const
	{
		return MP_uint32_reverse(*((quint32 *) (pkt + MP_ETH_IPV4_DST_OFF)));
	}
	inline quint16	tcp_get_r_src_port() const
	{
		return (*((quint16 *) (pkt + MP_ETH_IPV4_HDR_OFF + ip_get_header_len())));
	}
	inline quint8	tcp_get_flags() const
	{
		return (*((quint8 *)(pkt + MP_ETH_IPV4_HDR_OFF + ip_get_header_len() + 13)));
	}
	inline quint8	tcp_get_len() const
	{
		return (*((quint8 *)(pkt + MP_ETH_IPV4_HDR_OFF + ip_get_header_len() + 13)));
	}
	inline quint16	tcp_get_src_port() const
	{
		return MP_uint16_reverse(*((quint16 *) (pkt + MP_ETH_IPV4_HDR_OFF + ip_get_header_len())));
	}
	inline quint16	tcp_get_r_dst_port () const
	{
		return (*((quint16 *) (pkt + MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+2)));
	}
	inline quint16	tcp_get_dst_port () const
	{
		return MP_uint16_reverse(*((quint16 *) (pkt + MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+2)));
	}
	inline quint8	tcp_get_header_len()const
	{
		return (((*(quint8*)(pkt+MP_ETH_IPV4_HDR_OFF + ip_get_header_len()+12))>>4)<<2);
	}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////// MODBUS FUNCTIONS////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	inline quint16 modbus_get_transaction_ID() const
	{
		quint16 tid = *(quint16*)(pkt+MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+tcp_get_header_len());
		//tid =  MP_uint16_reverse(tid);
		return tid;
	}
	inline quint16 modbus_get_lenght()
	{
		quint16 len = *(quint16*)(pkt+MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+tcp_get_header_len()+4);
		return MP_uint16_reverse(len);
	}
	inline quint8 modbus_get_unitID()
	{
		return *(quint8*)(pkt+MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+tcp_get_header_len()+6);
	}
	inline quint8 modbus_get_fcnCode()
	{
		return *(quint8*)(pkt+MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+tcp_get_header_len()+7);
	}
	inline bool is_modbus_packet()
	{
		if(tcp_get_r_dst_port() == MP_REV_MODBUS_PORT || tcp_get_r_src_port() == MP_REV_MODBUS_PORT)
			return true;
		return false;
	}
	inline bool modbus_is_req()
	{
		if(tcp_get_r_dst_port() == MP_REV_MODBUS_PORT)
			return true;
		return false;
	}
	inline bool modbus_is_resp()
	{
		if(tcp_get_r_src_port() == MP_REV_MODBUS_PORT)
			return true;
		return false;
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////MODBUS REQUEST FUNCTIONS///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	inline void modbus_set_transaction_ID(quint16 tid)
	{
		*(quint16*)(pkt + MP_ETH_IPV4_HDR_OFF + ip_get_header_len() + tcp_get_header_len()) = tid;
	}
	inline quint16 modbus_req_get_address()
	{
		quint16 addr = *(quint16*)(pkt+MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+tcp_get_header_len()+ MP_MODBUS_TCP_HEADER_LEN+1);
		return MP_uint16_reverse(addr);
	}
	inline quint16 modbus_req_ReadWriteMultipleRegisters_get_read_addr()
	{
		quint16 addr = *(quint16*)(pkt+MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+tcp_get_header_len()+ MP_MODBUS_TCP_HEADER_LEN+1);
		return MP_uint16_reverse(addr);
	}
	inline quint16 modbus_req_ReadWriteMultipleRegisters_get_write_addr()
	{
		quint16 addr = *(quint16*)(pkt+MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+tcp_get_header_len()+ MP_MODBUS_TCP_HEADER_LEN+5);
		return MP_uint16_reverse(addr);
	}
	inline quint16 modbus_req_ReadWriteMultipleRegisters_get_read_quantity()
	{
		quint16 rq = *(quint16*)(pkt+MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+tcp_get_header_len()+ MP_MODBUS_TCP_HEADER_LEN+3);
		return MP_uint16_reverse(rq);
	}
	inline quint16 modbus_req_ReadWriteMultipleRegisters_get_write_quantity()
	{
		quint16 wq = *(quint16*)(pkt+MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+tcp_get_header_len()+ MP_MODBUS_TCP_HEADER_LEN+7);
		return MP_uint16_reverse(wq);
	}
	inline quint8 modbus_req_ReadWriteMultipleRegisters_get_byte_count()
	{
		return *(quint8*)(pkt+MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+tcp_get_header_len()+ MP_MODBUS_TCP_HEADER_LEN+9);
	}
	inline void modbus_req_ReadWriteMultipleRegisters_get_values(quint16 * values , quint8 byteCount)
	{
		memcpy(values , pkt+MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+tcp_get_header_len()+ MP_MODBUS_TCP_HEADER_LEN+10 , byteCount );
	}
	inline quint16 modbus_req_get_quantity()
	{
		quint16 q = *(quint16*)(pkt+MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+tcp_get_header_len()+ MP_MODBUS_TCP_HEADER_LEN+3);
		return MP_uint16_reverse(q);
	}
	inline quint8 modbus_req_multi_write_get_byteCount()
	{
		return *(quint8*)(pkt+MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+tcp_get_header_len()+ MP_MODBUS_TCP_HEADER_LEN+5);
	}
	inline quint16 modbus_req_single_write_get_value()
	{
		quint16 v = *(quint16*)(pkt+MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+tcp_get_header_len()+ MP_MODBUS_TCP_HEADER_LEN+3);
		return MP_uint16_reverse(v);
	}
	inline void modbus_req_Write_Multiple_Coils_get_value(quint8 * values , quint8 byte_count )
	{
		memcpy(values , pkt+MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+tcp_get_header_len()+ MP_MODBUS_TCP_HEADER_LEN+6 , byte_count );
	}
	inline void modbus_req_Write_Multiple_Registers_get_value(quint16 * values , quint8 byte_count )
	{
		memcpy(values , pkt+MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+tcp_get_header_len()+ MP_MODBUS_TCP_HEADER_LEN+6 , byte_count );
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////MODBUS RESPONSE FUNCTIONS////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	inline quint8 modbus_resp_get_byteCount()
	{
		return *(quint8 *)(pkt+MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+tcp_get_header_len()+ MP_MODBUS_TCP_HEADER_LEN + 1);
	}
	inline void modbus_resp_Read_Multiple_Registers_get_value(quint16 * values , quint8 byte_count )
	{
		memcpy(values , pkt+MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+tcp_get_header_len()+ MP_MODBUS_TCP_HEADER_LEN+2 , byte_count );
	}
	inline void modbus_resp_Read_Coils_DI_get_value(quint8 * values , quint8 byte_count )
	{
		memcpy(values , pkt+MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+tcp_get_header_len()+ MP_MODBUS_TCP_HEADER_LEN+2 , byte_count );
	}
	inline void modbus_resp_ReadWriteMultipleRegisters_get_values(quint16 * values , quint8 byteCount)
	{
		memcpy(values , pkt+MP_ETH_IPV4_HDR_OFF+ip_get_header_len()+tcp_get_header_len()+ MP_MODBUS_TCP_HEADER_LEN+2 , byteCount );
	}

};
