#pragma once
#include "MP_StateDescriptor.h"
#include "MP_Interface.h"
#include <QReadWriteLock>

class MP_StateMonitor
{
public:
	struct TransactionSelector
	{
		quint32 sIP;
		quint32 dIP;
		quint16 sPort;
		quint16 dPort;
		quint8	unitID;
		quint16 transID;
 		TransactionSelector():sIP(0),dIP(0),sPort(0),dPort(0),unitID(0),transID(0){}
 		TransactionSelector(quint32 p_sIP , quint32 p_dIP , quint16 p_sPort , quint16 p_dPort , quint8 p_unitID , quint16 p_transID):
 		sIP(p_sIP),dIP(p_dIP),sPort(p_sPort),dPort(p_dPort),unitID(p_unitID),transID(p_transID){}
		bool operator  < (const TransactionSelector &  n)const
		{
			if(transID < n.transID)
				return true;
			else if(transID > n.transID)
				return false;
			if(sPort < n.sPort)
				return true;
			else if(sPort > n.sPort)
				return false;
			if(dIP < n.dIP)
				return true;
			else if(dIP > n.dIP)
				return false;
			if(sIP < n.sIP)
				return true;
			else if(sIP > n.sIP)
				return false;
			if(dPort < n.dPort)
				return true;
			else if(dPort > n.dPort)
				return false;
			if(unitID < n.unitID)
				return true;
			else if(unitID > n.unitID)
				return false;
			return false;
		}

	};
	struct TransactionValues
	{
		quint16 StartingAddress;
		quint16 * RegisterValue;
		quint8 *  CoilValue;
		quint16 Quantity;
		quint16 SingleValue;
		quint8 ByteCount;
		quint16 ReadWriteReadQuantity;
		quint16 ReadWriteWriteQuantity;
		quint16 ReadWriteWriteStartingAddress;
		quint16 ReadWriteReadStartingAddress;

		TransactionValues():StartingAddress(0),RegisterValue(NULL),CoilValue(NULL),Quantity(0),SingleValue(0),ByteCount(0),
							ReadWriteReadQuantity(0), ReadWriteWriteQuantity(0),ReadWriteWriteStartingAddress(0),
							ReadWriteReadStartingAddress(0){}
	};
public:
	typedef QMap<TransactionSelector,TransactionValues> TransactionTable;

public:
	MP_StateDescriptor mStateDescriptor;
	TransactionTable mTransactionTable;
	quint16 mMaxHoldingRegisterValue;
	quint16 mMaxInputRegisterValue;
	QReadWriteLock mTransactionTableRWL;
	QMutex mSystemStateRWL;
	QString data;
	bool run_once;
	QString real_ds;
	quint16 tid;
public:
	MP_StateMonitor(void);
	~MP_StateMonitor(void);
	void MonitorAndUpdateState(MP_Packet * packet);
	void InsertTransaction(TransactionSelector sel , TransactionValues values);
	bool FindTransaction(MP_Packet * packet , TransactionValues & values);
	void RemoveTransaction(MP_Packet * packet);
	void test(QBitArray bits , bool test[8]);
	void ConstructDataSet(MP_Packet * packet);
	void real_data_set(MP_Packet * packet);
};
