#include "MP_Interface.h"
#include "Defs.h"
#include <QMutexLocker>
#include <process.h>

extern "C" void PacketCollector( u_char * iface , const struct pcap_pkthdr* pkthdr,const u_char * pkt )
{
	MP_Packet * packet = NULL;
	packet = (MP_Packet*)malloc(sizeof(MP_Packet));
	if(packet == NULL)
	{
		Q_ASSERT_X(false,"PacketCollector","cant malloc packet");
		return;
	}
	packet->pkt = NULL;
	packet->pkt = (unsigned char *)malloc(MP_MAX_PACKET_LEN);
	if(packet->pkt == NULL)
	{
		Q_ASSERT_X(false,"PacketCollector","cant malloc packet");
		free(packet);
		return;
	}
	memcpy(packet->pkt , pkt , MP_MAX_PACKET_LEN);
	MP_Interface * face = (MP_Interface *)iface;

	QMutexLocker ml(&face->mQueueMutex);//TODO: for compatibility, when multiple threads capture packets
	if (face->mPacketQueue.size() < MP_MAX_PACKET_QUEUE_SIZE)
	{
		face->mPacketQueue.enqueue(packet);
	}
	else
	{
		free(packet->pkt);
		free(packet);
		return;
	}
}
void StartCapture(void * par)
{
	MP_Interface * face = (MP_Interface *)par;
	pcap_loop(face->descr,-1,PacketCollector,(u_char*)par);
}

MP_Interface::MP_Interface(void):dev(NULL),descr(NULL),running(false)
{  
	dev = pcap_lookupdev(errbuf);//TODO
	if(dev == NULL)
	{
		Q_ASSERT_X(false,"in MP_Interface Constructor","cant find interface");
		exit(1); 
	}

	descr = pcap_open_live(dev,BUFSIZ,0,1,errbuf);
	if(descr == NULL)
	{
		Q_ASSERT_X(false,"in MP_Interface Constructor","cant open interface");
		exit(1); 
	}
}

MP_Interface::~MP_Interface(void)
{
}
MP_Packet * MP_Interface::ReadPacket(void)
{
	QMutexLocker ml(&mQueueMutex);
	if(mPacketQueue.isEmpty())
		return NULL;
	return mPacketQueue.dequeue();
}
void MP_Interface::init( void )
{
	uintptr_t thid = 0;
	thid = _beginthread(StartCapture,1024,this);
	running = true;
}


