#include "MP_StateMonitor.h"
#include <QBitArray>
#include <QFile>
#include <QTextStream>
MP_StateMonitor::MP_StateMonitor(void):mMaxHoldingRegisterValue(0),mMaxInputRegisterValue(0)
{
	mMaxInputRegisterValue = mStateDescriptor.mMaxInputRegisterValue;
	mMaxHoldingRegisterValue = mStateDescriptor.mMaxHoldingRegisterValue;
	data.append("WholeShutdown,SaftyInputValve,HPAlarm,SteamValve,SFR,Heater,InputValve,OutputValve,Temprature,Pressure,InputFlowRate,OutputFlowRate,TankLevel,TSP,LSP,TUpperBoundSP,TLowerBoundSP,LUpperBoundSP,LLowerBoundSP,TPGain,TIGain,TDGain,IN_LPGain,IN_LIGain,IN_LDGain,OUT_LPGain,OUT_LIGain,OUT_LDGain,TPIDOutputLowerBound,TPIDOutputUpperBound,IN_LPIDOutputLowerBound,IN_LPIDOutputUpperBound,OUT_LPIDOutputLowerBound,OUT_LPIDOutputUpperBound\n");
	run_once = false;
	tid = 1;
}

MP_StateMonitor::~MP_StateMonitor(void)
{
}

void MP_StateMonitor::MonitorAndUpdateState( MP_Packet * packet )
{
// 	if (packet->modbus_is_req())
// 		packet->modbus_set_transaction_ID(tid);
// 	else if (packet->modbus_is_resp())
// 		packet->modbus_set_transaction_ID(tid);
// 	QByteArray pktArray((char*)packet->pkt, packet->get_total_len());
// 	QString hex_pkt = pktArray.toHex();
// 	hex_pkt.append("\n");
// 
// 	QFile file("hex_dump.txt");
// 	if (!file.open(QIODevice::Append | QIODevice::Text))
// 	{
// 		return;
// 	}
// 	QTextStream out(&file);
// 	out << hex_pkt;
	if(packet->modbus_is_req())
	{
		TransactionSelector sel (packet->ip_get_r_src(),
			packet->ip_get_r_dst(),
			packet->tcp_get_r_src_port(),
			packet->tcp_get_r_dst_port(),
			packet->modbus_get_unitID(),
			packet->modbus_get_transaction_ID());

		TransactionValues values;
		values.StartingAddress = packet->modbus_req_get_address();

		MP_StateDescriptor::NodeSelector selector(packet->ip_get_dst(),packet->tcp_get_dst_port(),packet->modbus_get_unitID());

		quint8 function_code = packet->modbus_get_fcnCode();
		switch (function_code)
		{
		case MP_Packet::Read_Coils :
		case MP_Packet::Read_Discrete_Inputs:
		case MP_Packet::Read_Holding_Registers:
		case MP_Packet::Read_Input_Registers:
			{
				values.Quantity = packet->modbus_req_get_quantity();
			}
			break;
		case MP_Packet::Write_Single_Coil:
		case MP_Packet::Write_Single_Register:
			{
				values.SingleValue = packet->modbus_req_single_write_get_value();
			}
			break;
		case MP_Packet::Write_Multiple_Coils:
			{
				values.Quantity = packet->modbus_req_get_quantity();
				values.ByteCount = packet->modbus_req_multi_write_get_byteCount();
				values.CoilValue = new quint8[values.ByteCount];
				packet->modbus_req_Write_Multiple_Coils_get_value(values.CoilValue,values.ByteCount);
			}
			break;
		case MP_Packet::Write_Multiple_Registers:
			{
				values.Quantity = packet->modbus_req_get_quantity();
				values.ByteCount = packet->modbus_req_multi_write_get_byteCount();
				values.RegisterValue = new quint16[values.Quantity];
				packet->modbus_req_Write_Multiple_Registers_get_value(values.RegisterValue,values.ByteCount);

			}
			break;
		case MP_Packet::Read_Write_Multiple_registers:
			{
				values.ReadWriteReadQuantity = packet->modbus_req_ReadWriteMultipleRegisters_get_read_quantity();
				values.ReadWriteWriteQuantity = packet->modbus_req_ReadWriteMultipleRegisters_get_write_quantity();
				values.ReadWriteReadStartingAddress = packet->modbus_req_ReadWriteMultipleRegisters_get_read_addr();
				values.ReadWriteWriteStartingAddress = packet->modbus_req_ReadWriteMultipleRegisters_get_write_addr();
				values.ByteCount = packet->modbus_req_ReadWriteMultipleRegisters_get_byte_count();
				values.RegisterValue = new quint16[values.ReadWriteWriteQuantity];
				packet->modbus_req_ReadWriteMultipleRegisters_get_values(values.RegisterValue,values.ByteCount);
			}
			break;

		case MP_Packet::Mask_Write_Register:
			break;
		case MP_Packet::Diagnostics:
			break;
		case MP_Packet::Read_FIFO_Queue:
			break;
		case MP_Packet::Read_File_Record:
			break;
		case MP_Packet::Read_Exception_Status:
			break;
		case MP_Packet::Get_Comm_Event_Counter:
			break;
		case MP_Packet::Get_Comm_Event_Log:
			break;
		case  MP_Packet::Report_Server_ID:
			break;
		case MP_Packet::Write_File_Record:
			break;
		case MP_Packet::Encapsulated_Interface_Transport:
			break;
		default:Q_ASSERT(false);
		} 
		InsertTransaction(sel,values);
	}


	else if(packet->modbus_is_resp())
	{
		//QWriteLocker wl(&mSystemStateRWL);
		MP_StateDescriptor::NodeSelector selector(packet->ip_get_src(),packet->tcp_get_src_port(),packet->modbus_get_unitID());
		MP_StateDescriptor::SystemStateTable::Iterator primary_table_itr = mStateDescriptor.mSystemState.find(selector);
		if(primary_table_itr == mStateDescriptor.mSystemState.end())
			return;

		TransactionValues transVal;
		if(!FindTransaction(packet,transVal))
		{
			return;
		}
		//tid++;
		quint8 function_code = packet->modbus_get_fcnCode();
		quint8 code = function_code & MP_EXEPTION_RESPONSE_MASK;
		if(code == MP_NORMAL_RESPONSE)
		{
			switch (function_code)
			{
			case MP_Packet::Read_Coils :
				{
					quint8 ByteCount = packet->modbus_resp_get_byteCount();
					quint8 * value_arr = new quint8[ByteCount];
					QBitArray coils (transVal.Quantity);
					packet->modbus_resp_Read_Coils_DI_get_value(value_arr,ByteCount);
					quint16 num_coils = 1 ;
					for (int i = 0 ; i < ByteCount ; ++i)
					{
						quint8 tmp = value_arr[i];
						quint8 mask = 0x01;
						while(mask !=0)
						{
							if(num_coils > transVal.Quantity)
								break;
							int bit = tmp & mask;
							if(bit == 0)
								primary_table_itr->mCoils.clearBit(transVal.StartingAddress++);
							else
								primary_table_itr->mCoils.setBit(transVal.StartingAddress++);

							mask = mask << 1;
							num_coils++;
// 							if (bit ==0)
// 								real_ds.append("0,");
// 							else
// 								real_ds.append("1,");
							
						}
					}
// 					real_ds.remove(real_ds.lastIndexOf(","), 1);
// 					real_ds.append("\n");
					delete[] value_arr;

				}
				break;
			case MP_Packet::Read_Discrete_Inputs:
				{
					quint8 ByteCount = packet->modbus_resp_get_byteCount();
					quint8 * value_arr = new quint8[ByteCount];
					QBitArray di (transVal.Quantity);
					packet->modbus_resp_Read_Coils_DI_get_value(value_arr,ByteCount);
					quint16 num_DI = 1 ;
					for (int i = 0 ; i < ByteCount ; ++i)
					{
						quint8 tmp = value_arr[i];
						quint8 mask = 0x01;
						while(mask !=0)
						{
							if(num_DI > transVal.Quantity)
								break;
							int bit = tmp & mask;
							if(bit == 0)
								primary_table_itr->mDiscreteInputs.clearBit(transVal.StartingAddress++);
							else
								primary_table_itr->mDiscreteInputs.setBit(transVal.StartingAddress++);

							mask = mask << 1;
							num_DI++;
						}
					}
					delete[] value_arr;
					bool testt[8] ;
					test(primary_table_itr->mDiscreteInputs,testt);
				}
				break;
			case MP_Packet::Read_Holding_Registers:
				{
					quint8 ByteCount = packet->modbus_resp_get_byteCount();
					quint16 * value_arr = new quint16[transVal.Quantity];
					packet->modbus_resp_Read_Multiple_Registers_get_value(value_arr,ByteCount);

					for(int i = 0 ; i < transVal.Quantity ; ++i)
					{
						primary_table_itr->mHoldingRegisters[transVal.StartingAddress++] = MP_uint16_reverse(value_arr[i]);
						//real_ds.append(QString::number(MP_uint16_reverse(value_arr[i])) + ",");
					}
					delete [] value_arr;
				}
				break;
			case MP_Packet::Read_Input_Registers:
				{
					quint8 ByteCount = packet->modbus_resp_get_byteCount();
					quint16 * value_arr = new quint16[transVal.Quantity];
					packet->modbus_resp_Read_Multiple_Registers_get_value(value_arr,ByteCount);

					for(int i = 0 ; i < transVal.Quantity ; ++i)
					{
						primary_table_itr->mInputRegisters[transVal.StartingAddress++] = MP_uint16_reverse(value_arr[i]);
					}
					delete[] value_arr;
				}
				break;
			case MP_Packet::Write_Single_Coil:
				{
					if(transVal.SingleValue == MP_StateDescriptor::OFF)
						primary_table_itr->mCoils.clearBit(transVal.StartingAddress);
					else if(transVal.SingleValue == MP_StateDescriptor::ON)
						primary_table_itr->mCoils.setBit(transVal.StartingAddress);
				}
				break;
			case MP_Packet::Write_Single_Register:
				{
					primary_table_itr->mHoldingRegisters[transVal.StartingAddress] = transVal.SingleValue;
				}
				break;
			case MP_Packet::Write_Multiple_Coils:
				{
					QBitArray coils (transVal.Quantity);
					quint16 num_coils = 1 ;
					for (int i = 0 ; i < transVal.ByteCount ; ++i)
					{
						quint8 tmp = transVal.CoilValue[i];
						quint8 mask = 0x01;
						while(mask !=0)
						{
							if(num_coils > transVal.Quantity)
								break;
							int bit = tmp & mask;
							if(bit == 0)
								primary_table_itr->mCoils.clearBit(transVal.StartingAddress++);
							else
								primary_table_itr->mCoils.setBit(transVal.StartingAddress++);

							mask = mask << 1;
							num_coils++;
						}
					}
					delete[] transVal.CoilValue;
				}
				break;
			case MP_Packet::Write_Multiple_Registers:
				{
					for (int i = 0 ; i < transVal.Quantity ; ++i)
					{
						quint16 reg = MP_uint16_reverse(transVal.RegisterValue[i]);
						primary_table_itr->mHoldingRegisters[transVal.StartingAddress++] = MP_uint16_reverse(transVal.RegisterValue[i]);
					}
					delete [] transVal.RegisterValue;
				}
				break;
			case MP_Packet::Read_Write_Multiple_registers:
				{
					for (int i = 0 ; i < transVal.ReadWriteWriteQuantity ; ++i)
					{
						primary_table_itr->mHoldingRegisters[transVal.ReadWriteWriteStartingAddress++] = transVal.RegisterValue[i];
					}
					quint8 ByteCount = packet->modbus_resp_get_byteCount();
					quint16 * value_arr = new quint16[ByteCount];
					packet->modbus_resp_ReadWriteMultipleRegisters_get_values(value_arr,ByteCount);
					for (int i = 0 ; i < transVal.ReadWriteReadQuantity ; ++i)
					{
						primary_table_itr->mHoldingRegisters[transVal.ReadWriteReadStartingAddress++] = value_arr[i];
					}
					delete[] value_arr;
					delete [] transVal.RegisterValue;
				}
				break;
			case MP_Packet::Mask_Write_Register:
				break;
			case MP_Packet::Diagnostics:
				break;
			case MP_Packet::Read_FIFO_Queue:
				break;
			case MP_Packet::Read_File_Record:
				break;
			case MP_Packet::Read_Exception_Status:
				break;
			case MP_Packet::Get_Comm_Event_Counter:
				break;
			case MP_Packet::Get_Comm_Event_Log:
				break;
			case  MP_Packet::Report_Server_ID:
				break;
			case MP_Packet::Write_File_Record:
				break;
			case MP_Packet::Encapsulated_Interface_Transport:
				break;
			default:Q_ASSERT(false);
			}
		}
		else if(code == MP_EXEPTION_RESPONSE)
		{
		}
		RemoveTransaction(packet);
		ConstructDataSet(packet);
	}

	

}
void MP_StateMonitor::InsertTransaction( TransactionSelector sel , TransactionValues values )
{
	QWriteLocker ml(&mTransactionTableRWL);
 	mTransactionTable.insert(sel,values);
}

bool MP_StateMonitor::FindTransaction(MP_Packet * packet , TransactionValues & values )
{
	QReadLocker ml(&mTransactionTableRWL);
	if(mTransactionTable.isEmpty())
		return false;

	TransactionSelector sel (packet->ip_get_r_dst(),
		packet->ip_get_r_src(),
		packet->tcp_get_r_dst_port(),
		packet->tcp_get_r_src_port(),
		packet->modbus_get_unitID(),
		packet->modbus_get_transaction_ID());

	TransactionTable::Iterator itr = mTransactionTable.find(sel);
	if(itr == mTransactionTable.end())
		return false;

	values = *itr;
	return true;
}

void MP_StateMonitor::RemoveTransaction( MP_Packet * packet )
{
	QWriteLocker wl(&mTransactionTableRWL);
	if(mTransactionTable.isEmpty())
		return ;

	TransactionSelector sel (packet->ip_get_r_dst(),
		packet->ip_get_r_src(),
		packet->tcp_get_r_dst_port(),
		packet->tcp_get_r_src_port(),
		packet->modbus_get_unitID(),
		packet->modbus_get_transaction_ID());

	mTransactionTable.remove(sel);
}
void MP_StateMonitor::test( QBitArray bits , bool test[8] )
{
	for(int i = 0 ; i < 8 ; ++i)
	{
		test[i]=false;
		test[i] = bits.testBit(i);
	}
}

void MP_StateMonitor::ConstructDataSet(MP_Packet * packet)
{
	MP_StateDescriptor::SystemStateTable::Iterator primary_table_itr;
	MP_StateDescriptor::NodeSelector selector1(packet->ip_get_dst(), packet->tcp_get_dst_port(), packet->modbus_get_unitID());
	MP_StateDescriptor::NodeSelector selector2(packet->ip_get_src(), packet->tcp_get_src_port(), packet->modbus_get_unitID());
	if (packet->modbus_is_req())
	{
		primary_table_itr = mStateDescriptor.mSystemState.find(selector1);
	}
	else if (packet->modbus_is_resp())
	{

		primary_table_itr = mStateDescriptor.mSystemState.find(selector2);
	}

	if (primary_table_itr == mStateDescriptor.mSystemState.end())
		return;

	QVector<double> state;
	QVector<double> arr;
	QVector<double> ind;
	double max = 0;
	for (int i = 0; i < mStateDescriptor.mPrimaryFeatures.size(); ++i)
	{
		MP_StateDescriptor::PrimaryFeatures pf_slave = mStateDescriptor.mPrimaryFeatures.at(i);
		if (pf_slave.EssentialTablesNode == selector1 || pf_slave.EssentialTablesNode == selector2)
		{
			for (int j = 0; j < pf_slave.EssentialHR.size(); ++j)
			{
				state.append((double)primary_table_itr->mHoldingRegisters.at(pf_slave.EssentialHR.at(j)));
				
				if (!run_once)
					arr.append(1); ind.append(j);
			}

			for (int j = 0; j < pf_slave.EssentialIR.size(); ++j)
			{
				state.append((double)primary_table_itr->mInputRegisters.at(pf_slave.EssentialIR.at(j)));
				if (!run_once)
					arr.append(2); ind.append(j);
			}

			// 			QVector<double> tmp = state;
			// 			qSort(tmp.begin(), tmp.end());
			// 			max = tmp.last();
			// 			if (max == 0)
			// 				max = 1;
			// 
			// 			state[0] = state[0]
			// 			for (int j = 0; j < state.size(); ++j)
			// 			{
			// 				state[j] = state.at(j) / max;
			// 			}

			for (int j = 0; j < pf_slave.EssentialCO.size(); ++j)
			{
				state.append((double)primary_table_itr->mCoils.at(pf_slave.EssentialCO.at(j)));
				if (!run_once)
					arr.append(3); ind.append(j);
			}

			for (int j = 0; j < pf_slave.EssentialDI.size(); ++j)
			{
				
				state.append((double)primary_table_itr->mDiscreteInputs.at(pf_slave.EssentialDI.at(j)));
				if (!run_once)
					arr.append(4); ind.append(j);
			}
		}
	}
	QString state_str;
	QString arr_str;
	QString ind_str;
	for (int i = 0; i < state.size(); ++i)
	{
		state_str.append(QString::number(state.at(i)) + ",");
	}
	state_str.remove(state_str.lastIndexOf(","), 1);
	state_str.append("\n");
	if (!run_once)
	{
		for (int i = 0; i < arr.size(); ++i)
		{
			arr_str.append(QString::number(arr.at(i)) + ",");
		}
		arr_str.remove(arr_str.lastIndexOf(","), 1);
		arr_str.append("\n");
		for (int i = 0; i < ind.size(); ++i)
		{
			ind_str.append(QString::number(ind.at(i)) + ",");
		}
		ind_str.remove(ind_str.lastIndexOf(","), 1);
		ind_str.append("\n");
		QString arr_ind = arr_str + ind_str;

		QFile file2("arr_index.csv");
		if (!file2.open(QIODevice::Append | QIODevice::Text))
		{
			return;
		}
		QTextStream out2(&file2);
		out2 << arr_ind;
		arr_ind = "";
		run_once = true;
	}

	QFile file("DS.csv");
	if (!file.open(QIODevice::Append | QIODevice::Text))
	{
		return;
	}
 	QTextStream out(&file);
 	out << state_str;
 	state_str = "";


// 	QFile file("real_DS.csv");
// 	if (!file.open(QIODevice::Append | QIODevice::Text))
// 	{
// 		return;
// 	}
//  	QTextStream out(&file);
//  	out << real_ds;
//  	real_ds = "";











// 
// 	data.append(QString::number(primary_table_itr->mCoils.at(WholeShutdown)) + ",");
// 	data.append(QString::number(primary_table_itr->mDiscreteInputs.at(SaftyInputValve)) + ",");
// 	data.append(QString::number(primary_table_itr->mDiscreteInputs.at(HPAlarm)) + ",");
// 	data.append(QString::number(primary_table_itr->mDiscreteInputs.at(SteamValve)) + ",");
// 	data.append(QString::number(primary_table_itr->mInputRegisters.at(SFR)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(Heater)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(InputValve)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(OutputValve)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(Temprature)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(Pressure)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(InputFlowRate)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(OutputFlowRate)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(TankLevel)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(TSP)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(LSP)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(TUpperBoundSP)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(TLowerBoundSP)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(LUpperBoundSP)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(LLowerBoundSP)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(TPGain)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(TIGain)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(TDGain)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(IN_LPGain)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(IN_LIGain)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(IN_LDGain)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(OUT_LPGain)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(OUT_LIGain)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(OUT_LDGain)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(TPIDOutputLowerBound)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(TPIDOutputUpperBound)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(IN_LPIDOutputLowerBound)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(IN_LPIDOutputUpperBound)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(OUT_LPIDOutputLowerBound)) + ",");
// 	data.append(QString::number(primary_table_itr->mHoldingRegisters.at(OUT_LPIDOutputUpperBound))+ "\n");
// 	QFile file2("DS_for_baysian.csv");
// 	if (!file2.open(QIODevice::Append | QIODevice::Text))
// 	{
// 		return;
// 	}
// 	QTextStream out2(&file2);
// 	out2 << data;
// 	data = "";
	
}
void MP_StateMonitor::real_data_set(MP_Packet * packet)
{


}