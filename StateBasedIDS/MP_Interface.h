#pragma once
#define HAVE_REMOTE
#include <pcap.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <QQueue>
#include <QMutex>
#include <QVector>
#include "MP_Packet.h"
class MP_Interface
{
public:
	QQueue<MP_Packet *> mPacketQueue;
	char *dev;
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t* descr;
	QMutex mQueueMutex;
	bool running;
public:
	MP_Interface(void);
	~MP_Interface(void);
	MP_Packet * ReadPacket(void);
	void init(void);
};
