#include "MP_StateBasedIDSCore.h"
#include "MP_StateBasedIDS.h"
#include "MP_ParallelRun.h"
MP_StateBasedIDSCore::MP_StateBasedIDSCore(int &argc, char **argv):QCoreApplication(argc,argv),mStateBasedIDS(NULL),mInterface(NULL),mParallelRun(NULL)
{
	init();
}

MP_StateBasedIDSCore::~MP_StateBasedIDSCore(void)
{
	delete mInterface;
	delete mStateBasedIDS;
	delete mParallelRun;
}

void MP_StateBasedIDSCore::init()
{
	mInterface = new MP_Interface();
	mStateBasedIDS = new MP_StateBasedIDS(mInterface);
	mParallelRun = new MP_ParallelRun(mStateBasedIDS);
	mParallelRun->begin();
}
