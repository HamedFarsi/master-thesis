#include <QStringList>
#include <QFile>
#include <QString>
#include <QHostAddress>
#include "MP_CriticalState.h"
#include "Defs.h"
#pragma warning(disable:4996)

MP_CriticalState::MP_CriticalState(void)
{
	QStringList rules_list;
	QFile CSFile(MP_INI_CRITICAL_STATE_PATH);
	quint16 i = 0 ;
	bool new_host = false;
	QStringList rule_parts , ip_port , condition_parts;
	CSRuleStruct cs_rule;
	MP_StateDescriptor::NodeSelector selector;
	PureRule rule_part;
	QVector<PureRule> rule;
	if (CSFile.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		while (!CSFile.atEnd()) 
		{
			QByteArray line = CSFile.readLine();
			rules_list.append(QString(line));
		}
		while(i < rules_list.size())
		{
			rules_list[i] = rules_list[i].trimmed();
			rules_list[i].remove(QChar(']'));
			rules_list[i].remove(QChar('['));
			if (rules_list[i].toLower() == "slave")
			{
				++i;
				new_host = true;
				continue;
			}
			while (i < rules_list.size() && rules_list.at(i).toLower() != "slave" && rules_list.at(i) != "\n")
			{
				if (new_host)
				{
					ip_port = rules_list[i].split(":");
					selector.IP = QHostAddress(ip_port[0].trimmed()).toIPv4Address();
					selector.Port = ip_port[1].trimmed().toUShort();
					selector.unitID = ip_port[2].trimmed().toUShort();
					++i;
					new_host = false;
					continue;
				}
				
				rule_parts = rules_list[i].split("&");
				for (int j = 0 ; j < rule_parts.size() ; ++j)
				{
					rule_parts[j] = rule_parts[j].trimmed();
					condition_parts = rule_parts[j].split(" ");

					QString table_type = condition_parts[0].trimmed().left(2);
					if(table_type == "CO")
						rule_part.sTableType = MP_CriticalState::CO;	
					else if(table_type == "HR")
						rule_part.sTableType = MP_CriticalState::HR;
					else if(table_type == "DI")
						rule_part.sTableType = MP_CriticalState::DI;
					else if (table_type == "IR")
						rule_part.sTableType = MP_CriticalState::IR;

					rule_part.sIndex = condition_parts[0].trimmed().mid(2).remove(']').remove('[').toUShort();
					if(condition_parts[1] == ">")
						rule_part.sOp = MP_CriticalState::Larger;
					else if(condition_parts[1] == ">=")
						rule_part.sOp = MP_CriticalState::LargerAndEqual;
					else if(condition_parts[1] == "<")
						rule_part.sOp = MP_CriticalState::Smaller;
					else if(condition_parts[1] == "<=")
						rule_part.sOp = MP_CriticalState::SmallerAndEqual;
					else if(condition_parts[1] == "=")
						rule_part.sOp = MP_CriticalState::Equall;
					else if(condition_parts[1] == "!=")
						rule_part.sOp = MP_CriticalState::NotEquall;

					rule_part.sConstraint = condition_parts[2].trimmed().toUShort();
					rule.push_back(rule_part);
					
				}
				cs_rule.CSRule.push_back(rule);
				rule.clear();
				++i;
			}
			mCriticalStates.insert(selector,cs_rule);
			cs_rule.CSRule.clear();
			++i;
		}
	}
}

MP_CriticalState::~MP_CriticalState(void)
{
}


