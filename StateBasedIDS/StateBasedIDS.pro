#-------------------------------------------------
#
# Project created by QtCreator 2015-04-11T15:23:27
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = StateBasedIDS
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app
win32: INCLUDEPATH += ./Include
win32: LIBS += Packet.lib
win32: LIBS += wpcap.lib

SOURCES += main.cpp

SOURCES += MP_Interface.cpp
HEADERS += MP_Interface.h

SOURCES += MP_Packet.cpp
HEADERS += MP_Packet.h

SOURCES += MP_StateBasedIDS.cpp
HEADERS += MP_StateBasedIDS.h

SOURCES += MP_StateDescriptor.cpp
HEADERS += MP_StateDescriptor.h

SOURCES += MP_StateMonitor.cpp
HEADERS += MP_StateMonitor.h

SOURCES += MP_ParallelRun.cpp
HEADERS += MP_ParallelRun.h

SOURCES += MP_StateBasedIDSCore.cpp
HEADERS += MP_StateBasedIDSCore.h

SOURCES += MP_CriticalState.cpp
HEADERS += MP_CriticalState.h

QMAKE_CXXFLAGS += /MP



HEADERS += Defs.h

