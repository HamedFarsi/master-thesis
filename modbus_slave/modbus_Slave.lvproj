﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Property Name="varPersistentID:{17462285-D523-4CD7-B7FE-EF802862657A}" Type="Ref">/My Computer/Variables.lvlib/OutputFlowRate</Property>
	<Property Name="varPersistentID:{206BC6F2-6F65-412A-94F8-2864440D6F79}" Type="Ref">/My Computer/Variables.lvlib/BottomHeater</Property>
	<Property Name="varPersistentID:{29107250-4C1D-4DE8-8843-32F74B818315}" Type="Ref">/My Computer/Variables.lvlib/BoilerTankLevel</Property>
	<Property Name="varPersistentID:{2F0DE364-1576-4E99-A687-C6C3DBC3F177}" Type="Ref">/My Computer/Variables.lvlib/MiddleHeater</Property>
	<Property Name="varPersistentID:{3A6DE3D4-A7B4-47A0-B22E-83A8DFF6368F}" Type="Ref">/My Computer/Variables.lvlib/OutputFlowMeter</Property>
	<Property Name="varPersistentID:{41AD7C97-A581-43EF-AAEA-D96C0EFD05DE}" Type="Ref">/My Computer/Variables.lvlib/InputFlowRate</Property>
	<Property Name="varPersistentID:{47125F9F-18F8-48A8-96D3-E5DC6901E9F5}" Type="Ref">/My Computer/Variables.lvlib/Pressure</Property>
	<Property Name="varPersistentID:{68E9DEA5-5193-4768-AF25-BC41504CDAFA}" Type="Ref">/My Computer/Variables.lvlib/TopHeater</Property>
	<Property Name="varPersistentID:{8C487A93-12DD-4B83-8AE3-DFEC8F81A045}" Type="Ref">/My Computer/Variables.lvlib/Temperature</Property>
	<Property Name="varPersistentID:{900D3B2C-D5BA-4782-A593-B1D88486FC5A}" Type="Ref">/My Computer/Variables.lvlib/InputFlowMeter</Property>
	<Property Name="varPersistentID:{95E08A85-6E04-410C-B508-55DF1BE26EBF}" Type="Ref">/My Computer/Variables.lvlib/OutputSteam</Property>
	<Property Name="varPersistentID:{A7B5906D-5DCB-4885-A18D-9C236C96C47D}" Type="Ref">/My Computer/Variables.lvlib/SteamControlValve</Property>
	<Property Name="varPersistentID:{AAAF5986-4FF7-4549-8346-A8F91C46B161}" Type="Ref">/My Computer/Variables.lvlib/WholeShutdown</Property>
	<Property Name="varPersistentID:{DA40F6D4-5AF5-4525-9D13-3F0F9426EBC9}" Type="Ref">/My Computer/Variables.lvlib/SaftyInputValve</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="BoilerCore.vi" Type="VI" URL="../BoilerCore.vi"/>
		<Item Name="BoilerPLC.vi" Type="VI" URL="../BoilerPLC.vi"/>
		<Item Name="S.vi" Type="VI" URL="../S.vi"/>
		<Item Name="SFR.vi" Type="VI" URL="../SFR.vi"/>
		<Item Name="WT2SP.vi" Type="VI" URL="../WT2SP.vi"/>
		<Item Name="Variables.lvlib" Type="Library" URL="../Variables.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="lvpidtkt.dll" Type="Document" URL="/&lt;vilib&gt;/addons/control/pid/lvpidtkt.dll"/>
				<Item Name="MB Globals.vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Globals.vi"/>
				<Item Name="NI_PID__prctrl compat.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/control/pid/NI_PID__prctrl compat.lvlib"/>
				<Item Name="NI_PID_pid.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/control/pid/NI_PID_pid.lvlib"/>
				<Item Name="MB Registers Manager.vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Registers Manager.vi"/>
				<Item Name="MB Slave Read All Input Registers (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Slave Read All Input Registers (poly).vi"/>
				<Item Name="MB Slave Read All Holding Registers (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Slave Read All Holding Registers (poly).vi"/>
				<Item Name="MB Slave Read All Discrete Inputs (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Slave Read All Discrete Inputs (poly).vi"/>
				<Item Name="MB Slave Read All Coils (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Slave Read All Coils (poly).vi"/>
				<Item Name="MB Slave Write Input Registers (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Slave Write Input Registers (poly).vi"/>
				<Item Name="MB Slave Write Holding Registers (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Slave Write Holding Registers (poly).vi"/>
				<Item Name="MB Slave Write Discrete Inputs (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Slave Write Discrete Inputs (poly).vi"/>
				<Item Name="MB Slave Write Coils (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Slave Write Coils (poly).vi"/>
				<Item Name="MB Slave Read Input Registers (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Slave Read Input Registers (poly).vi"/>
				<Item Name="MB Slave Read Holding Registers (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Slave Read Holding Registers (poly).vi"/>
				<Item Name="MB Slave Read Discrete Inputs (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Slave Read Discrete Inputs (poly).vi"/>
				<Item Name="MB Slave Read Coils (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Slave Read Coils (poly).vi"/>
				<Item Name="MB Slave Init (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Slave Init (poly).vi"/>
				<Item Name="MB Slave Operations (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Slave Operations (poly).vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
